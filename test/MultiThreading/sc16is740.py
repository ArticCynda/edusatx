#!/usr/bin/python3

import smbus2 as smbus # necessary for I2C access
import RPi._GPIO as GPIO # for interrupts
from time import sleep
from datetime import datetime
from nSentencer import NmeaSentencer

#sc16is740_addr  = 0x4C
#int_pin         = 8 # this is GPIO14

# registers
reg_RHR         = 0x00 << 3
reg_THR         = 0x00 << 3
reg_IER         = 0x01 << 3
reg_FCR         = 0x02 << 3
reg_IIR         = 0x02 << 3
reg_LCR         = 0x03 << 3
reg_MCR         = 0x04 << 3
reg_LSR         = 0x05 << 3
reg_MSR         = 0x06 << 3
reg_SPR         = 0x07 << 3
reg_TCR         = 0x06 << 3
reg_TLR         = 0x07 << 3
reg_TXLVL       = 0x08 << 3
reg_RXLVL       = 0x09 << 3
#reg_IODir       = 0x0A << 3                         # not available on SC16IS740
#reg_IOState     = 0x0B << 3                         # not available on SC16IS740
#reg_IOIntEna    = 0x0C << 3                         # not available on SC16IS740
#reg_IOControl   = 0x0E << 3                         # not available on SC16IS740
reg_EFCR        = 0x0F << 3

# special registers
reg_DLL = 0x00 << 3                                  # division registers MUST be set to control the baud rate divisor
reg_DLH = 0x01 << 3

# enhanced register set
reg_EFR     = 0x02 << 3
reg_XON1    = 0x04 << 3
reg_XON2    = 0x05 << 3
reg_XOFF1   = 0x06 << 3
reg_XOFF2   = 0x07 << 3

# bit encoding
bit7 = 0x80
bit6 = 0x40
bit5 = 0x20
bit4 = 0x10
bit3 = 0x08
bit2 = 0x04
bit1 = 0x02
bit0 = 0x01


# status bits
IER_int_dis                 = 0                 # disable all interrupts
IER_CTS_int_en              = bit7              # CTS interrupt enable
IER_RTS_int_en              = bit6              # RTS interrupt enable
IER_Xoff                    = bit5              # Xoff interrupt enable
IER_Sleep_mode              = bit4              # sleep mode enable
IER_modem_status_int        = bit3              # modem status interrupt enable
IER_receive_line_status_int = bit2              # recieve line status interrupt enable
IER_THR_empty_int           = bit1              # transmit holding register interrupt enable
IER_RX_data_available_int   = bit0              # receive holding register interrupt enable

FCR_RX_trig_lvl_8char       = 0                 # set RX trigger level to 8 characters
FCR_RX_trig_lvl_16char      = bit6              # set RX trigger level to 16 characters
FCR_RX_trig_lvl_56char      = bit7              # set RX trigger level to 56 characters
FCR_RX_trig_lvl_60char      = bit6 | bit7       # set RX trigger level to 60 characters
FCR_TX_trig_lvl_8spaces     = 0                 # set TX trigger level to 8 spaces
FCR_TX_trig_lvl_16spaces    = bit4              # set TX trigger level to 16 spaces
FCR_TX_trig_lvl_32spaces    = bit5              # set TX trigger level to 32 spaces
FCR_TX_trig_lvl_56spaces    = bit4 | bit5       # set TX trigger level to 56 spaces
FCR_TX_FIFO_RST             = bit2              # reset TX FIFO
FCR_RX_FIFO_RST             = bit1              # reset RX FIFO
FCR_FIFO_en                 = bit0              # FIFO enable
FCR_FIFO_dis                = 0                 # FIFO disable

IIR_FIFO_en                 = bit7              # mirror of contents in FCR0
IIR_FIFO2_en                = bit6              # mirror of contents in FCR0
IIR_RX_line_status_err      = bit2 | bit1       # reciever line status error
IIR_RX_time_out_int         = bit3 | bit2       # receiver time-out interrupt
IIR_RHR_int                 = bit2              # RHR interrupt
IIR_THR_int                 = bit1              # THR interrput
IIR_modem_int               = 0                 # modem interrupt
IIR_input_pin_change_state  = bit5 | bit4       # input pin change state
IIR_Xoff_spec_char          = bit4              # received Xoff signal or special character
IIR_CTS_RTS_inactive        = bit5              # CTS, RTS change of state from active low to inactive high
IIR_int_status              = bit0              # interrupt status

LCR_div_latch_en            = bit7              # divisor latch enable
LCR_set_break               = bit6              # break control bit, when enabled causes a break condition to be transmitted (TX forced to logic 0 state)
LCR_no_parity               = 0                 # no parity
LCR_odd_parity              = bit3              # odd parity
LCR_even_parity             = bit3 | bit4       # even parity
LCR_forced_parity_1         = bit3 | bit5       # forced parity '1'
LCR_forced_parity_0         = bit3 | bit4 | bit5# forced parity '0'
LCR_stop_bit_1              = 0                 # 1 stop bit
LCR_stop_bits_15            = bit2              # 1.5 stop bits
LCR_stop_bits_2             = bit2              # 2 stop bits
LCR_word_length_5           = 0                 # set word length to 5
LCR_word_length_6           = bit0              # set word length to 6
LCR_word_length_7           = bit1              # set word length to 7
LCR_word_length_8           = bit0 | bit1       # set word length to 8

MCR_clock_divisor_1         = 0                 # divide clock by 1
MCR_clock_divider_4         = bit7              # divide clock by 4
MCR_IrDA_mode               = bit6              # IrDA mode
MCR_UART_mode               = 0                 # UART mode
MCR_Xon_any                 = bit5              # Xon Any
MCR_loopback_en             = bit4              # loopback enabled, the MCR signals are looped into MSR and TX output is looped back to RX input
MCR_loopback_dis            = 0                 # loopback disabled
MCR_TCR_TLR_en              = bit2              # TCR and TLR enable
MCR_RTS                     = bit1              # force RTS output active low
MCR_DTR                     = bit0              # force DTR output active low

LSR_FIFO_data_err           = bit7              # FIFO data error
LSR_THR_TSR_empty           = bit6              # THR and TSR empty (bit is the transmit empty indicator)
LSR_THR_TSR_empty           = bit5              # THR empty (bit is the transmit holding register empty indicator)
LSR_break_interrupt         = bit4              # break interrupt
LSR_framing_err             = bit3              # framing error
LSR_parity_err              = bit2              # parity error
LSR_overrun_err             = bit1              # overrun error
LSR_data_in_receiver        = bit0              # data in receiver

MSR_dis                     = 0                 # disable modem status register
MSR_CD                      = bit7              # CD active high, GPIO6
MSR_RI                      = bit6              # RI active high, GPIO7
MSR_DSR                     = bit5              # DSR active high, GPIO4
MSR_CTS                     = bit4              # CTS ative high
MSR_dCD                     = bit3              # indicates CD has changed
MSR_dRI                     = bit2              # indicates RI has changed
MSR_dDSR                    = bit1              # indicates DSR has changed, cleared on read
MSR_dCTS                    = bit0              # indicates CTS has changed, cleared on read

EFCR_IrDA_mode_3_16         = 0                 # set IrDA pulse ratio to 3/16, up to 115.2 kbits/s
EFCR_IrDA_mode_1_4          = bit7              # set IrDA pulse ratio to 1/4, up to 1.152 Mbits/s
EFCR_auto_RS485_RTS_inv     = bit5              # set RTS inversion in RS-485 mode
EFCR_auto_RS485_RTS_con     = bit4              # enable the transmitter to control the RTS pin
EFCR_TX_dis                 = bit2              # disable transmitter (FIFO can still be filled up by host)
EFCR_RX_dis                 = bit1              # disable receiver
EFCR_9bit_mode_en           = bit0              # enable 9-bit or multidrop mode in RS-485

EFR_auto_CTS                = bit7              # CTS flow control enable
EFR_auto_RTS                = bit6              # RTS flow control enable
EFR_special_char_det        = bit5              # special character detection enable
EFR_enhanced_func_en        = bit4              # enhanced functions enable
EFR_no_TX_flow_ctrl         = 0                 # no transmit flow control
EFR_transmit_Xon1_Xoff1     = bit3              # transmit Xon1, Xoff1
EFR_transmit_Xon2_Xoff2     = bit2              # transmit Xon2, Xoff2
EFR_transmit_Xon12_Xoff12   = bit2 | bit3       # transmit Xon1 and Xon2, Xoff1 and Xoff2
EFR_no_RX_flow_ctrl         = 0                 # no received flow control
EFR_rec_comp_Xon1_Xoff1     = bit1              # receiver compares Xon1, Xoff1
EFR_rec_comp_Xon2_Xoff2     = bit0              # receiver compares Xon2, Xoff2
EFR_rec_TX_comp1            = bit3 | bit1|bit0  # transmit Xon1, Xoff1, receiver compares Xon1 or Xon2, Xoff1 or Xoff2
EFR_rec_TX_comp2            = bit2 | bit1|bit0  # trasnmit xon2, Xoff2, receiver compares Xon1 or Xon2, Xoff1 or Xoff2
EFR_rec_TX_comp3            = bit3 | bit2|bit1 |bit0 #transmit Xon1 and Xon2, Xoff1 and Xoff2, receiver compares Xon1 and Xon2, Xoff1 and Xoff2
EFR_no_transmit_flow_ctrl   = bit1 | bit0       # no transmit flow control, receiver compares Xon1 and Xon2, Xoff1 and Xoff2     

initialRegDict = { reg_IER: IER_RX_data_available_int,
                   reg_FCR: FCR_FIFO_en               | FCR_RX_trig_lvl_8char | FCR_TX_trig_lvl_8spaces | FCR_RX_FIFO_RST | FCR_TX_FIFO_RST,
                   reg_LCR: LCR_no_parity             | LCR_stop_bit_1        | LCR_word_length_8,
                   reg_MCR: MCR_clock_divisor_1       | MCR_UART_mode         | MCR_loopback_dis
                
                   #reg_MSR: MSR_dis,
                   #reg_EFCR: 0,
                   #reg_DLL: 0,
                   #reg_DLH: 0,
                   #reg_FCR: 0,
                   #reg_TCR: 0,
                   #reg_EFR: EFR_no_RX_flow_ctrl        | EFR_no_TX_flow_ctrl
                   }

baudrates = { 1.8432: { 50:     2304,
                        75:     1536,
                        110:    1047,
                        134.5:  857,
                        150:    768,
                        300:    384,
                        600:    192,
                        1200:   96,
                        1800:   64,
                        2000:   58,
                        2400:   48,
                        3600:   32,
                        4800:   24,
                        7200:   16,
                        9600:   12,
                        19200:  6,
                        38400:  3,
                        56000:  2},
              3.072: {  50:     2304,
                        75:     2560,
                        110:    1745,
                        134.5:  1428,
                        150:    1280,
                        300:    640,
                        600:    320,
                        1200:   160,
                        1800:   107,
                        2000:   96,
                        2400:   80,
                        3600:   53,
                        4800:   40,
                        7200:   27,
                        9600:   20,
                        19200:  10,
                        38400:  5}}

class SC16IS740:
    """ Device driver for the SC16IS740 I2C to UART Bridge IC """
    
    def __init__(self, addr = 0x4C, intPin = 8, baudRate = 9600, crystal = 1.8432):
        """ Instance constructor """
        sleep(0.003)                                # wait 3 µs for POR to complete in case init happens immediately after power-up
        self.bus             = smbus.SMBus(1)       # connect to I2C bus on GPIO pins
        self._sc16is740_addr = addr                 # the I2C address of the device, this is 0x4C by default
        self._initialiseRegisters()                 # set up the configuration registers
        self._initPin(intPin)                       # setup GPIO pin
        self._intPin = intPin
        if not self.selfTest():
            raise Exception('SC16IS740: Failed Self Test')
        self.setBaudrate(baudRate, crystal)
        
    def _initialiseRegisters(self):
        """ Initialise registers with their default values to configure the transceiver hardware """
        for reg in initialRegDict.keys():
            self.bus.write_byte_data(self._sc16is740_addr, reg, initialRegDict[reg])

    def _initPin(self, intPin):
        """ Set up external GPIO interrupt on the specified GPIO pin. """

        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(intPin, GPIO.IN)
        GPIO.remove_event_detect(intPin)
        #GPIO.add_event_detect(intPin, GPIO.FALLING, callback=lambda channel:self._ISR(channel))
        
    def selfTest(self):
        """ Self test writes a known value to the Scratch Pad Register (SPR), which doesn't affect other functions on the chip
            then attempts to read it back and compare. Returns a value indicating whether the self test passed or not.
            If the self test passes, it indicates the chip is powered up, wired to the I2C bus correctly, and responding properly. """
        
        # define magic value and write it to the SPR register
        magic_value = 0x42
        self._writeByte(reg_SPR,magic_value)

        # read register back and compare with magic value
        return_value = self._readByte(reg_SPR)  
        return (magic_value == return_value)

    def setBaudrate(self, baudrate, crystal):
        """ Sets the baud rate for the transceiver to the specified value for either a 1.8432 MHz or 3.072 MHz crystal.
            If the specified baud rate or crystal frequency is not supported, will default to 9600 bps at 1.8432 MHz. """

        self._baudrate = baudrate
        self._crystal = crystal
        value = None
        try:
            value = baudrates[crystal][baudrate]
        except:
            # no divisor value available for the specified baud rate, default to 9600 bps
            if (crystal == 1.8432):
                value = 12
            elif (crystal == 3.072):
                value = 20
            else:
                # invalid crystal frequency specified
                # will assume 1.8 MHz for lack of better alternative options
                value = 12

        # set the divisor registers
        MSB = ((value & 0xFF00) >> 8) & 0xFF
        LSB = (value & 0xFF)

        # unlock divisor registers:
        LCR_value = self.bus.read_byte_data(self._sc16is740_addr, reg_LCR)
        new_LCR_value = LCR_value | LCR_div_latch_en
        self._writeByte(reg_LCR, new_LCR_value)
        self._writeByte(reg_DLH, MSB)
        self._writeByte(reg_DLL, LSB)
        self._writeByte(reg_LCR, LCR_value)

    def getBaudRate(self):
        """ Read baud rate from device """

        # unlock divisor registers:
        LCR_value = self._readByte(reg_LCR)
        new_LCR_value = LCR_value | LCR_div_latch_en
        self._writeByte(reg_LCR, new_LCR_value)

        MSB = self._readByte(reg_DLH)
        LSB = self._readByte(reg_DLL)

        self._writeByte(reg_LCR, LCR_value)
        divider = MSB << 8 | LSB

        key_list = list(baudrates[self._crystal].keys())
        val_list = list(baudrates[self._crystal].values())

        # look up retrieved divisor value in the baud rate dictionary
        bd = None
        try:
            pos = val_list.index(divider)
            bd = key_list[pos]
        except:
            print('invalid divider value: ' + str(divider))

        if (bd == self._baudrate):
            pass
        else:
            raise Exception('baudrate read back failed, mismatch: ' + str(bd) + ' vs ' + str(self._baudrate))        
        return bd

    def _readByte(self, reg):
        """ Read a single byte from the specified register. """
        return self.bus.read_byte_data(self._sc16is740_addr, reg)

    def _writeByte(self, reg, value):
        """ Write a single byte to the specified register """
        self.bus.write_byte_data(self._sc16is740_addr, reg, value)

    def dataAvailable(self, wait = True):
        """ block until data available then return nb bytes available
        """
        if wait:
            GPIO.wait_for_edge(self._intPin, GPIO.FALLING)
        return self._readByte(reg_RXLVL)

    def getDataBlock(self):
        bytes_to_read = self._readByte(reg_RXLVL) # check how many bytes need to be read
        return self.bus.read_i2c_block_data(self._sc16is740_addr, reg_RHR, min(32, bytes_to_read))
    
    def _readIntBits(self):
        """ Read interrupt bits from the most important registers. This is only relevant for debugging. """

        reg_value = self._readByte(reg_IIR)
        print('IIR status bits: ', bin(reg_value))

        reg_value = self._readByte(reg_IER)
        print('IER status bits: ', bin(reg_value))

        reg_value = self._readByte(reg_LSR)
        print('LSR status bits: ', bin(reg_value))


if __name__ == '__main__':
    sc = SC16IS740()
    print(sc.getBaudRate())


