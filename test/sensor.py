#!/usr/bin/python3


class Sensor:
    """ A general Sensor Framework Parent class
    which can be inherited into child classes to 
    provide structural support.
    """
    class FailedSelfTest(Exception):
        """ raise this exception in case of failed self test
        """
        def __str__(self):
            return  str(self.caller) + ': Failed Self Test'

        def __init__(self,_caller):
            Exception.__init__(self)
            self.caller = _caller
    
    def selfTest(self):
        return True

    def __init__(self,name):
        self.whoAmI = name
        
    def __str__(self):
        return self.whoAmI


class SpaceSensor(Sensor):
    """ An example of a concrete sensor class making use of the parent
    and the FaliedSelfTest exception
    """
    def selfTest(self):
        if not self.ok:
            raise Sensor.FailedSelfTest(self)

    def __init__(self):
        Sensor.__init__(self,'The Space Sensor')
        self.ok = False

class PerfectSensor(Sensor):
    """ An example of a concrete sensor class making use of the parent
    selfTest method.
    """
    def __init__(self):
        Sensor.__init__(self,'The Perfect Sensor')

def runSome():
    try:
        sp = SpaceSensor()
        print('SapceSensor selfTest: ' + str(sp.selfTest()))
    except Exception as e:
        print('Exception caught:' + str(e))
        print('Here, if we can fix what went wrong, we do, otherwise we give up...')
    try:        
        ps =  PerfectSensor()
        print('Perfect Sensor selfTest: ' + str(ps.selfTest()))
    except Exception as e:
        print('Exception caught:' + str(e))
        print('Here, if we can fix it, we do, otherwise we give up...')
    finally:
        print ('Here we close open files, the I2C bus or whatever to leave'
               ' the systm in a clean  state')
        print('Now, we can exit and feel good about it!')

if __name__ =='__main__':
    runSome()
            
        
