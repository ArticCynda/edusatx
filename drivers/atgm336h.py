#!/usr/bin/python3

from sc16is740 import SC16IS740 # sc16is740 #.sc16is740
import pynmea2
from datetime import datetime, timedelta
import time
from sensor import Sensor

i2c_addr = 0x4c
int_pin = 8

latlong_init_value = 200
satCount_init_value = -1
altitude_init_value = -1

"""
Example message set from ATGM336H:

$GNGGA,222036.000,5126.66109,N,00233.27071,W,1,08,1.4,75.2,M,0.0,M,,*53
$GNGLL,5126.66109,N,00233.27071,W,222036.000,A,A*5B
$GPGSA,A,3,07,10,18,20,23,26,,,,,,,3.3,1.4,3.0*3C
$BDGSA,A,3,27,46,,,,,,,,,,,3.3,1.4,3.0*22
$GPGSV,3,1,09,07,17,321,20,08,20,276,,10,24,143,33,16,77,207,24*7D
$GPGSV,3,2,09,18,45,059,27,20,41,107,32,23,37,110,24,26,47,163,35*76
$GPGSV,3,3,09,27,59,283,*40
$BDGSV,1,1,04,11,11,043,,13,34,046,,27,63,069,22,46,30,201,29*6A
$GNRMC,222036.000,A,5126.66109,N,00233.27071,W,0.00,143.48,090121,,,A*6D
$GNVTG,143.48,T,,M,0.00,N,0.00,K,A*29
$GNZDA,222036.000,09,01,2021,00,00*46
$GPTXT,01,01,01,ANTENNA OK*35
"""

class ATGM336H(Sensor):
    """ Device driver from the ATGM336H GNSS navigation module. """

    measures = ['coordinates', 'altitude', 'time', 'satellite count']

    def __init__(self):
        """ Instance constructor """
        Sensor.__init__(self, 'GNSS')
        method_list = [self.getLocation, self.getAltitude, self.getTime, self.getSatelliteCount]
        for (key, value) in map(lambda x,y: (x,y),ATGM336H.measures,method_list):
            self.measuresDict[key] = value
        
            
        #print(self.measuresDict)

        self._transceiver = SC16IS740()
        if (self._transceiver.selftest() == 0):
            print('SC16IS740 self test failed, check GNSS module is wired up correctly')
            raise IOError

        if (self._selfTest() == False):
            print('Unable to initialise the GNSS module, check wiring and antenna, and try again.')
            raise IOError

        self._latitute = latlong_init_value     # initialise latitude
        self._longitude = latlong_init_value    # initialise longitude
        self._satCount = satCount_init_value    # initialise satellite count
        self._time = 0                          # time
        self._altitude = altitude_init_value    # altitude


    def _selfTest(self):
        """ Checks the ATGM336H module by analysing its antenna status message. """

        msg = ''
        start_time = time.time()
        found = False
        while (not found):
            msg = self._transceiver.readLine()
            try:
                if (msg.startswith('$GPTXT')):
                    found = True
            except:
                pass
                
            # if (msg != None):
            #     print(msg)


            
            if (time.time() - start_time > 10 ):
                # if more than 10 seconds passed without detecting a $GPTXT message, there is a problem with the module
                # $GPTXT message should come in every second
                print('No incoming data from GNSS module, check wiring.')
                return False

        if ('ANTENNA OK' in msg):
            print('GNSS receiver up and running, antenna connected')
            return True
        elif ('ANTENNA OPEN' in msg):
            print('No antenna connected to GNSS module. Please connect antenna.')
            return False
        elif ('ANTENNA SHORT' in msg):
            print('Incorrect antenna type or hardware fault. Please replace antenna.')
            return False

            
    def _readNMEAMessage(self):
        """ Read a single NMEA message from the module. """
        return self._transceiver.readLine()

    def _decodeNMEAMessage(self, msg):
        """ Decode a NMEA message to retrieve location and motion parameters. """
        return pynmea2.parse(msg)

    def getLocation(self):
        """ Returns [latitude, longitude] coordinates at the current position. """
        self._updateNMEAvariables()
        while (self._latitute == latlong_init_value or self._longitude == latlong_init_value):
            self._updateNMEAvariables()

        return [self._latitute, self._longitude]
        

    def getAltitude(self):
        """ Returns the altitude above sea level at the current position. """
        self._updateNMEAvariables()
        while (self._altitude == altitude_init_value):
            self._updateNMEAvariables()

        return self._altitude

    def getSatelliteCount(self):
        """ Returns the number satellites in sight, across all 5 constellations. """
        self._updateNMEAvariables()
        while (self._satCount == satCount_init_value):
            self._updateNMEAvariables()

        return self._satCount

    def getTime(self):
        """ Returns the time. """
        self._updateNMEAvariables()
        while (self._time == 0):
            self._updateNMEAvariables()

        return self._time

    def _updateNMEAvariables(self):
        """ The ATGM336H module sends position updates every second, in a burst of 12 messages.
            Need to retrieve the last 12 strings and parse them to make sure the variables are up-to-date. """
        
        for k in range(12):
            message = None
            while not message:
                message = self._readNMEAMessage()  # read a NMEA message from the serial buffer
            NMEAmsg = self._decodeNMEAMessage(message)

            # Not every NMEA message returned by the ATGM336H contains all information, so need to decode it and assign selectively
            try:
                if (NMEAmsg.timestamp != None):
                    self._time = NMEAmsg.timestamp
            except:
                pass
            
            try:
                if (NMEAmsg.latitude != None and NMEAmsg.longitude != None):
                    self._latitute = round(float(NMEAmsg.latitude), 5)
                    self._longitude = round(float(NMEAmsg.longitude), 5)
            except:
                pass

            try:
                if (NMEAmsg.num_sats != None and NMEAmsg.latitude != None):
                    self._satCount = int(NMEAmsg.num_sats)
            except:
                pass

            try:
                if (NMEAmsg.altitude != None):
                    self._altitude = float(NMEAmsg.altitude)
            except:
                pass


    
    def __str__(self):
        return  'coordinates: [' + str(self._latitute) + ', ' + str(self._longitude) + ']\naltitude: ' + str(self._altitude)  + '\nsat count: ' +  str(self._satCount) + '\ntime: ' + str(self._time)  

if __name__ == "__main__":
    
    atgm = ATGM336H()
    atgm._selfTest()

    atgm._updateNMEAvariables()
    print(atgm)





