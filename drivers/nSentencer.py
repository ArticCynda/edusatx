#!/usr/bin/python3

from threading import Lock

class NmeaSentencer:
    """ Maintains an iterable for NMEA sentences based on lists of
    ascii values representing characters and/or <LF> <CR>
    """

    EOS = ord('\n')
    
    def __init__(self, lis = []):
        self.sentenceList = []
        self.rest = []
        self.parseIncoming(lis)

    def __iter__(self):
        return self

    def __next__(self):
        """ std iterator method used to get next value,
        In our case, the value obtained is removed from the instance.
        Partial sentences are not returned.
        """
        res = self.sentenceList[0]
        self.sentenceList = self.sentenceList[1:]
        return res

    def parseIncoming(self,lis):
        """ parse incoming list of ascii values that represent either
        zero or more complete NMEA sentences followed by zero or more 
        incomplete NMEA sentences.
        results of parsing updates instance memeber variables appropiately
        """
        if lis:
            index = -1
            try:
                index = lis.index(NmeaSentencer.EOS)
            except ValueError:
                pass
            if index !=-1:
                self.sentenceList.append(''.join([chr(x) for x in self.rest + lis[:index]]).strip())
                self.rest = []
                self.parseIncoming(lis[index+1:])
            else:
                self.rest += lis

    def __str__(self):
        return 'Sentence List: ' + str(self.sentenceList) + '\n' + 'Rest: ' + (str(self.rest) if self.rest else '<empty>')                 
        
if __name__ == '__main__':
    s = NmeaSentencer()
    sent0 =  [ord(x) for x in '$GPS,1,2,3,*45\r\n']
    sent1 =  [ord(x) for x in '$GPS,6,7']
    sent2 =  [ord(x) for x in '*25\r\n']
    for c in sent0 +sent1 + sent2:
        s.parseIncoming([c])
    print('\nAfter parsing that, our instance now looks like this:')
    print(s)
    print('\nNow let\'s interate')
    i = iter(s)
    while True:
        try:
            print (next(i))
        except IndexError:
            break
    print ('Done')
    
    print ( 'now another..')
    s = NmeaSentencer()
    for c in [sent0,sent1,sent2]:
        s.parseIncoming(c)
    print('\nAfter parsing that, our instance now looks like this:')
    print(s)
    print('\nNow let\'s interate')
    i = iter(s)
    while True:
        try:
            print (next(i))
        except IndexError:
            break
    
    
